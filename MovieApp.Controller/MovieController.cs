using Microsoft.AspNetCore.Mvc;
using MovieApp.Repository.Dto;
using MovieApp.Service;
using Newtonsoft.Json;

namespace MovieApp.Controller
{
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly IMovieQueryService movieQueryService;

        public MovieController(IMovieQueryService _movieQueryService)
        {
            movieQueryService = _movieQueryService;
        }

        [HttpGet]
        [Route("isalive")]
        public IActionResult IsAlive()
        {
            return new OkObjectResult("Alive");
        }

        [HttpGet]
        [Route("movie/genre/{genre}")]
        public IActionResult GetMovieByGenre(Genre genre)
        {
            var result = JsonConvert.SerializeObject(movieQueryService.GetByGenre(genre));
            return new OkObjectResult(result);
        }
    }
}