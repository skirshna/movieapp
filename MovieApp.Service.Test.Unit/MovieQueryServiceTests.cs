using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using FluentAssertions;
using MovieApp.Repository;
using MovieApp.Repository.Dto;
using NUnit.Framework;

namespace MovieApp.Service.Test.Unit
{
    public class MovieQueryServiceTests
    {
        [Test]
        public void GivenValidGenre_WhenGetByGenre_ReturnsListOfMoviesWithActorInformation()
        {
            var TestGenre = Genre.Comedy;

            var actor = new ActorDto {Id = Guid.Empty, Name = "AnActor", PhotoLocation = "FileLoc"};
            IList<ActorDto> actorList = new List<ActorDto>
                {actor};

            var movie = new MovieDto {Id = Guid.Empty, Name = "AMovie", TimeDuration = "2hrs", Genre = TestGenre};
            var movieList = new List<MovieDto> {movie};

            var actorRepo = ActorQueryRepository(actor);
            var movieRepo = MovieQueryRepository(movieList);

            var service = new MovieQueryService(movieRepo, actorRepo);
            var result = service.GetByGenre(TestGenre);

            result.First().Name.Should().Be("AMovie");
            result.First().Genre.Should().Be(TestGenre);
            result.First().Actors.First().Name.Should().Be("AnActor");
        }

        private static IMovieQueryRepository MovieQueryRepository(List<MovieDto> movieList)
        {
            var movieRepo = A.Fake<IMovieQueryRepository>();
            A.CallTo(() => movieRepo.GetByGenre(A<Genre>.Ignored)).Returns(movieList);
            return movieRepo;
        }

        private static IActorQueryRepository ActorQueryRepository(ActorDto actor)
        {
            var actorRepo = A.Fake<IActorQueryRepository>();
            A.CallTo(() => actorRepo.GetActor(A<Guid>.Ignored)).Returns(actor);
            A.CallTo(() => actorRepo.GetActorsByMovie(A<Guid>.Ignored))
                .Returns(new List<Guid> {actor.Id});
            return actorRepo;
        }
    }
}