using System;
using System.Collections.Generic;
using System.Linq;

namespace MovieApp.Repository.Dto
{
    public static class SampleData
    {
        private static IList<MovieDto> Movies = GetSampleMovies();
        private static readonly IList<ActorDto> Actors = GetSampleActors();

        private static IList<ActorDto> GetSampleActors()
        {
            var actorList = new List<ActorDto>();
            for (var i = 0; i < 5; i++)
                actorList.Add(new ActorDto
                    {Id = Guid.NewGuid(), Name = $"Actor_{i}", PhotoLocation = $"s3://path_{i}"});

            return actorList;
        }

        public static Dictionary<Guid, IList<Guid>> GetMovieActorMapping()
        {
            var mapper = new Dictionary<Guid, IList<Guid>>
            {
                {Movies[0].Id, new List<Guid> {Actors[1].Id, Actors.First().Id}},
                {Movies[1].Id, new List<Guid> {Actors[2].Id, Actors[3].Id, Actors.Last().Id}},
                {Movies[2].Id, new List<Guid> {Actors[2].Id, Actors[3].Id, Actors.Last().Id}},
                {Movies[3].Id, new List<Guid> {Actors[4].Id, Actors[1].Id, Actors.First().Id}}
            };
            return mapper;
        }

        public static IList<ActorDto> PopulateActors()
        {
            return Actors;
        }

        public static IList<MovieDto> PopulateMovies()
        {
            return Movies;
        }

        private static IList<MovieDto> GetSampleMovies()
        {
            Movies = new List<MovieDto>
            {
                new() {Id = Guid.NewGuid(), Name = "Fargo", Genre = Genre.Thriller},
                new() {Id = Guid.NewGuid(), Name = "HomeAlone", Genre = Genre.Comedy},
                new() {Id = Guid.NewGuid(), Name = "HomeAlone - 2", Genre = Genre.Comedy},
                new()
                {
                    Id = Guid.NewGuid(), Name = "Avengers", Genre = Genre.Action
                    //ActorsIds = Actors.Select(a => a.Id).ToList()
                }
            };

            return Movies;
        }
    }
}