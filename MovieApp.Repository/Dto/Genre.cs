namespace MovieApp.Repository.Dto
{
    public enum Genre
    {
        Action,
        Comedy,
        Thriller
    }
}