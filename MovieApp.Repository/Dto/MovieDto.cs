using System;

namespace MovieApp.Repository.Dto
{
    public class MovieDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Genre Genre { get; set; }
        public string TimeDuration { get; set; }
    }
}