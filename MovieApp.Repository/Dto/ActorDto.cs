using System;

namespace MovieApp.Repository.Dto
{
    public class ActorDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PhotoLocation { set; get; }
    }
}