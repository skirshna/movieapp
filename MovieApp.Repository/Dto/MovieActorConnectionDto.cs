using System;
using System.Collections.Generic;

namespace MovieApp.Repository.Dto
{
    public class MovieActorConnectionDto
    {
        public Guid MovieId { set; get; }
        public IList<Guid> ActorId { set; get; }
    }
}