using System;
using System.Collections.Generic;
using MovieApp.Repository.Dto;

namespace MovieApp.Repository
{
    public class ActorQueryRepository : IActorQueryRepository
    {
        private readonly IList<ActorDto> Actors;

        public ActorQueryRepository()
        {
            Actors = SampleData.PopulateActors();
        }

        public ActorDto GetActor(Guid ActorId)
        {
            foreach (var a in Actors)
                if (a.Id.Equals(ActorId))
                    return a;
            return null;
        }

        public IList<Guid> GetActorsByMovie(Guid movieId)
        {
            var mapper = SampleData.GetMovieActorMapping();
            return mapper[movieId];
        }
    }
}