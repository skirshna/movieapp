using System.Collections.Generic;
using MovieApp.Repository.Dto;

namespace MovieApp.Repository
{
    public interface IMovieQueryRepository
    {
        public IList<MovieDto> GetByGenre(Genre genre);
        public MovieDto GetById(string id);
    }
}