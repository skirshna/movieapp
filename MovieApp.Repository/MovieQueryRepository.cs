using System.Collections.Generic;
using System.Linq;
using MovieApp.Repository.Dto;

namespace MovieApp.Repository
{
    public class MovieQueryRepository : IMovieQueryRepository
    {
        private readonly IList<MovieDto> sampleMovies;

        public MovieQueryRepository()
        {
            //usually we will have db connection injected here. 
            sampleMovies = SampleData.PopulateMovies();
        }

        public IList<MovieDto> GetByGenre(Genre genre)
        {
            return sampleMovies.Where(m => m.Genre == genre).ToList();
        }

        public MovieDto GetById(string movieId)
        {
            foreach (var m in sampleMovies)
                if (m.Id.Equals(movieId))
                    return m;
            return null;
        }
    }
}