using System;
using System.Collections.Generic;
using MovieApp.Repository.Dto;

namespace MovieApp.Repository
{
    public interface IActorQueryRepository
    {
        public ActorDto GetActor(Guid Id);
        public IList<Guid> GetActorsByMovie(Guid movieId);
    }
}