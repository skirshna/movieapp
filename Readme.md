### Movie App

Movie App is a sample .net core web app that host information about `Movies ` and `Actors`.

#### Code Structure:
Code structure goes like this : Controller -> Service Layer -> Repository Layer
- Controller has a health check endpoint and an API to get movies by Genre.
- Service layer deals with the business logic of retriving movie details by genre, and also is responsible for
mapping the lists of Actors for each Movie object. This logic is unit tested.
- Repository layer consists of query repositories for Actor and Movie. For the purpose of this exercise, 
repository is mocked with in-memory data. 

### API Endpoint

#### 1. GET /isalive

ResponBody: Alive

#### 2. GET /movie/genre/{genre}

Get Movie and Actor List by Genre. 
Currently supports Action, Comedy and Thriller.

Sample Response Body: 
```angular2html
[
  {
     Id: '9a06b424-c0d4-495b-8eff-e9c5f6a4817f',
     Name: 'movie_name',
     TimeDuration: '2 hours',
     Genre: 'Comedy',
     Actors: [
       { 'Id':'b00483a4-a63f-4fb4-bda2-37a5b8a9dc96',
         'Name':"Actor_2",
         'PhotoLocation':'s3://path_2'
       }
     ]
  }
]
```
Status Code: 200 OK

Error Code: 400 for bad request

### Framework
- .Net core 5.0

### Test dependencies
- .Nunit
- FakeItEasy
- FluentAssertions
