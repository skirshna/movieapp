using System.Collections.Generic;
using MovieApp.Repository.Dto;
using MovieApp.Service.Domain;

namespace MovieApp.Service
{
    public interface IMovieQueryService
    {
        public IList<Movie> GetByGenre(Genre genre);
    }
}