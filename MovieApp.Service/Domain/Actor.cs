using System;

namespace MovieApp.Service.Domain
{
    public class Actor
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PhotoLocation { set; get; }
    }
}