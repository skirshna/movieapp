using System;
using System.Collections.Generic;
using MovieApp.Repository.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MovieApp.Service.Domain
{
    public class Movie
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Genre Genre { get; set; }

        public string TimeDuration { get; set; }
        public IList<Actor> Actors { get; set; }
    }
}