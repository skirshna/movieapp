using System.Collections.Generic;
using MovieApp.Repository.Dto;

namespace MovieApp.Service.Domain
{
    public static class DomainMapper
    {
        public static Movie ToMovies(this MovieDto movieDto, IList<ActorDto> actorDtos)
        {
            var actorList = new List<Actor>();
            foreach (var a in actorDtos) actorList.Add(a.ToActor());
            var movie = new Movie
            {
                Id = movieDto.Id,
                Name = movieDto.Name,
                TimeDuration = movieDto.TimeDuration,
                Genre = movieDto.Genre,
                Actors = actorList
            };

            return movie;
        }

        private static Actor ToActor(this ActorDto actorDto)
        {
            return new()
            {
                Id = actorDto.Id,
                Name = actorDto.Name,
                PhotoLocation = actorDto.PhotoLocation
            };
        }
    }
}