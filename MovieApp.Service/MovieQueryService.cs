using System;
using System.Collections.Generic;
using MovieApp.Repository;
using MovieApp.Repository.Dto;
using MovieApp.Service.Domain;

namespace MovieApp.Service
{
    public class MovieQueryService : IMovieQueryService
    {
        private readonly IActorQueryRepository ActorQueryRepository;
        private readonly IMovieQueryRepository MovieQueryRepository;

        public MovieQueryService(IMovieQueryRepository _movieQueryRepository,
            IActorQueryRepository _actorQueryRepository)
        {
            MovieQueryRepository = _movieQueryRepository;
            ActorQueryRepository = _actorQueryRepository;
        }

        public IList<Movie> GetByGenre(Genre genre)
        {
            var moviesList = new List<Movie>();
            var moviesDto = MovieQueryRepository.GetByGenre(genre);

            foreach (var m in moviesDto)
            {
                var actorsDtoList = GetActorsForMovie(m.Id);
                moviesList.Add(m.ToMovies(actorsDtoList));
            }

            return moviesList;
        }

        private IList<ActorDto> GetActorsForMovie(Guid movieId)
        {
            var actorDtoList = new List<ActorDto>();
            var actorIds = ActorQueryRepository.GetActorsByMovie(movieId);
            foreach (var actorId in actorIds)
            {
                var actorDto = ActorQueryRepository.GetActor(actorId);
                actorDtoList.Add(actorDto);
            }

            return actorDtoList;
        }
    }
}